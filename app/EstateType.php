<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstateType extends Model
{
    protected $fillable = [
        'name',
    ];

    public function estates()
    {
        return $this->hasMany(Estates::class);
    }
}
