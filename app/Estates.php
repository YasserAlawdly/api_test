<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estates extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }

    public function likes()
    {
       return $this->morphMany(Like::class , 'likable');
    }

    public function advertisements()
    {
        return $this->hasMany(Advertisement::class);
    }

    public function estateType()
    {
        return $this->belongsTo(EstateType::class , 'estateType_id');
    }
}
