<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
        'name', 'user_id' , 'en' , 'ar',
    ];
    public function user()
    {
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }
}
