<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $fillable = [
        'name' , 'estate_id', 'en' , 'ar',
    ];

    public function estate()
    {
        return $this->belongsTo(Estates::class , 'estate_id' , 'id');
    }
}
