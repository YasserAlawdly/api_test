<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Estates extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'name' => $this->name,
            'user' => $this->user()->get(),
            'likes' => $this->likes()->get()
        ];
    }

    public function with($request)
    {
        return [
            'status' => true
        ];
    }
}
