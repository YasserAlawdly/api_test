<?php

namespace App\Http\Resources;

use App\Like;
use http\Env\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);

        return [
            'email' => $this->email,
            'name' => $this->name,
            'likes' => $this->likes()->get()
        ];
    }

    public function with($request)
    {
        return [
            'status' => true,
        ];
    }

}
