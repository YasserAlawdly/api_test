<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth' , 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function verify($token)
    {
        $user  = User::where('verify_token' , $token)->first();
        if ($user){
            if ($user->verify == 0){
                $user->verify = 1;
                $user->save();
                return redirect('home');
            }elseif ($user->verify == 1){
                echo "Your email already verified";
            }else {
                abort(404);
            }
        }else{
            abort(404);
        }
    }
}
