<?php

namespace App\Http\Controllers;

use App\EstateType;
use Illuminate\Http\Request;

class EstateTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estateTypes = EstateType::paginate(5);

        return view('dashboard.EstateType.index' , compact('estateTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estateType = new EstateType();

        return view('dashboard.EstateType.create' , compact('estateType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        EstateType::create($validate);

        return redirect(route('EstateType.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estateType =EstateType::findOrFail($id);

        return view('dashboard.EstateType.edit' , compact('estateType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estateType =EstateType::findOrFail($id);

        $validate = $request->validate([
            'name' => 'required',
        ]);

        $estateType->update($validate);

        return redirect(route('EstateType.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estateType =EstateType::findOrFail($id);
        $estateType->delete();
        return redirect(route('EstateType.index'));
    }
}
