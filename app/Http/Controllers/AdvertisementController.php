<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Estates;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisements = Advertisement::paginate(5);

        return view('dashboard.Advertisement.index' , compact('advertisements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estates = Estates::all()->pluck('name' , 'id')->prepend('none');
        $advertisement = new Advertisement();

        return view('dashboard.Advertisement.create' , compact('estates' , 'advertisement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'estate_id' => 'required|exists:estates,id',
            'name' => 'required',
            'en' => 'required',
            'ar' => 'required',
        ]);

        Advertisement::create($validate);

        return redirect(route('advertisement.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        $estates = Estates::all()->pluck('name' , 'id')->prepend('none');

        return view('dashboard.Advertisement.edit' , compact('advertisement' , 'estates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {

        $validate = $request->validate([
            'estate_id' => 'required|exists:estates,id',
            'name' => 'required',
            'en' => 'required',
            'ar' => 'required'
        ]);

        $advertisement->update($validate);

        return redirect(route('advertisement.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        $advertisement->delete();
        return redirect(route('advertisement.index'));
    }
}
