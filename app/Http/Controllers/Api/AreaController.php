<?php

namespace App\Http\Controllers\Api;

use App\Area;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Area as AreaResource;

class AreaController extends Controller
{
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        Area::create([
            'name' => $validate['name'],
            'user_id' => auth()->user()->id,
            'en' => 'required',
            'ar' => 'required',
        ]);

        return response()->json(['message' => 'Added Successfully'] , 200);
    }


    public function show($locale ,$id)
    {

        \App::setlocale($locale);

        $area = Area::find($id);

        if (\App::getLocale() == 'en'){
            return response()->json(['Lang' => $area->en , 'data' => new AreaResource($area)]);
        }elseif (\App::getLocale() == 'ar'){
            return response()->json(['Lang' => $area->ar , 'data' => new AreaResource($area)]);
        }else{
            return response()->json(['Lang' => 'no language selected']);
        }
    }


    public function update(Request $request, Area $area)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        $area->update([
            'name' => $validate['name'],
            'user_id' => auth()->user()->id,
            'en' => 'required',
            'ar' => 'required',
        ]);


        return response()->json(['message' => 'Updated Successfully'] , 200);

    }


    public function destroy(Area $area)
    {
        $area->delete();

        return response()->json(['message' => 'Deleted Successfully'] , 200);
    }
}
