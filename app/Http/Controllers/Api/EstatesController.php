<?php

namespace App\Http\Controllers\Api;
use App\Estates;
use App\Http\Resources\Estates as EstatesResource;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EstatesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function show($id){

        $estate = Estates::find($id);

        return new EstatesResource($estate);
    }

    public function store(Request $request){

        $data = $request->validate([
           'name' => 'required',
        ]);

        $data['user_id'] = auth()->user()->id;
        $estate =Estates::create($data);

        return new EstatesResource($estate);
    }

    public function like($id)
    {
        $estate = Estates::find($id);

        $estate->likes()->create();

        return new EstatesResource($estate);

    }
}
