<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Estates;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Advertisement as AdvertisementResource;

class AdvertisementController extends Controller
{

    public function store(Request $request)
    {
        $validate = $request->validate([
            'estate_id' => 'required|exists:estates,id',
            'name' => 'required',
            'en' => 'required',
            'ar' => 'required',
        ]);

        Advertisement::create($validate);

        return response()->json(['message' => 'Added Successfully'] , 200);
    }


    public function show($locale ,$id)
    {
        \App::setlocale($locale);

        $advertisement = Advertisement::find($id);

        if (\App::getLocale() == 'en'){
            return response()->json(['Lang' => $advertisement->en , 'data' => new AdvertisementResource($advertisement)]);
        }elseif (\App::getLocale() == 'ar'){
            return response()->json(['Lang' => $advertisement->ar , 'data' => new AdvertisementResource($advertisement)]);
        }else{
            return response()->json(['Lang' => 'no language selected']);
        }

    }


    public function update(Request $request, Advertisement $advertisement)
    {
        $validate = $request->validate([
            'estate_id' => 'required|exists:estates,id',
            'name' => 'required',
            'en' => 'required',
            'ar' => 'required',
        ]);

        $advertisement->update($validate);

        return response()->json(['message' => 'Updated Successfully'] , 200);

    }


    public function destroy(Advertisement $advertisement)
    {
        $advertisement->delete();

        return response()->json(['message' => 'Deleted Successfully'] , 200);
    }
}
