<?php

namespace App\Http\Controllers\Api;

use App\EstateType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\EstateType as EstateTypeResource;
class EstateTypeController extends Controller
{
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        EstateType::create($validate);

        return response()->json(['message' => 'Added Successfully'] , 200);
    }


    public function show($id)
    {
        $estateType = EstateType::find($id);

        return new EstateTypeResource($estateType);
    }


    public function update(Request $request, $id)
    {
        $estateType =EstateType::findOrFail($id);

        $validate = $request->validate([
            'name' => 'required',
        ]);

        $estateType->update($validate);

        return response()->json(['message' => 'Updated Successfully'] , 200);

    }


    public function destroy($id)
    {
        $estateType =EstateType::findOrFail($id);
        $estateType->delete();

        return response()->json(['message' => 'Deleted Successfully'] , 200);
    }
}
