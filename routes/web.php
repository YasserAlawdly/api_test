<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

//['verify' => true]

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' =>['auth' , 'auth.admin'] , 'prefix' => 'dashboard'] , function (){
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::resource('advertisement', 'AdvertisementController');
    Route::resource('EstateType', 'EstateTypeController');
    Route::resource('areas', 'AreaController');
});

Route::get('/verify/{token}' , 'HomeController@verify')->name('verify');

Route::get('/lang/{locale}' , function ($locale){
    session()->put('locale' , $locale);
    return redirect()->back()->withInput();
})->name('language');
