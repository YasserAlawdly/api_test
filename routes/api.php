<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'auth' , 'namespace' => '\App\Http\Controllers\Api'] , function(){

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    Route::get('/email/resend', 'VerificationController@resend')->name('verification.resend');
    Route::get('/email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');
});


Route::group(['prefix' => '{locale}' ,'middleware' =>['auth:api' , 'auth.admin:api'] , 'namespace' => '\App\Http\Controllers\Api'] , function (){
    Route::resource('advertisement', 'AdvertisementController');
    Route::resource('area', 'AreaController');
    Route::resource('estateType', 'EstateTypeController');
});

Route::get('estates/{estates}' , 'Api\EstatesController@show');
Route::post('estates' , 'Api\EstatesController@store');
Route::post('estates/{estates}' , 'Api\EstatesController@like');
