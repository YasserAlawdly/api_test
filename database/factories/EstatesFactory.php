<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Estates::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user_id' => \App\User::all()->random()->id,
        'estateType_id' => \App\EstateType::all()->random()->id,
    ];
});
