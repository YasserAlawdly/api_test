<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EstateType;
use Faker\Generator as Faker;

$factory->define(EstateType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
