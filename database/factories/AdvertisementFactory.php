<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Advertisement;
use Faker\Generator as Faker;

$factory->define(Advertisement::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'estate_id' => \App\Estates::all()->random()->id,
    ];
});
