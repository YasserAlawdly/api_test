<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Like::class, function (Faker $faker) {
    return [
        'likable_id' => \App\User::all()->random()->id,
        'likable_type' => 'App\User'
    ];
});
