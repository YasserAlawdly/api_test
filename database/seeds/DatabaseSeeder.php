<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\EstateType::class, 3)->create();
//        factory(App\User::class, 50)->create();
        factory(App\Estates::class, 10)->create();
//        factory(App\Advertisement::class, 10)->create();
//        factory(App\Like::class, 20)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
