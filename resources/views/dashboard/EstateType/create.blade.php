@extends('layouts.dashboard')

@section('content')
    <div class="col">
        <form action="{{ route('EstateType.store') }}" method="POST">

            @include('dashboard.EstateType.fields');

            <div class="form-group row">
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary">{{ __('lang.addEstateType') }}</button>
                </div>
                <div class="col-sm-9">
                    <a href="{{ route('EstateType.index') }}" class="btn btn-secondary">{{ __('lang.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
@endsection
