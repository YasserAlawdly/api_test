@extends('layouts.dashboard')

@section('buttons')
    <a class="btn btn-primary" href="{{route('EstateType.create')}}" role="button">{{ __('lang.addNewEstateType') }}</a>
@endsection

@section('content')
    <table class="table">
        <thead>
        <tr>
            <td>EstateType ID</td>
            <td>EstateType Name</td>
            <th class="Actions">{{ __('lang.actions') }}</th>
        </tr>
        </thead>
        <body>
        @foreach($estateTypes as $estateType)
            <tr>
                <td>{{ $estateType->id }}</td>
                <td>{{ $estateType->name }}</td>
                <td class="actions">
                    <a
                        href="{{ route('EstateType.edit', ['EstateType' => $estateType->id]) }}"
                        alt="Edit"
                        title="Edit">
                        {{ __('lang.edit') }}
                    </a>
                    <form action="{{ route('EstateType.destroy', ['EstateType' => $estateType->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-link" title="Delete" value="Delete">{{ __('lang.delete') }}</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </body>
    </table>
    {{ $estateTypes->links() }}
@endsection

