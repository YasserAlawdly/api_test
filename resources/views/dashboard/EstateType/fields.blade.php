@if($errors->any())
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
    @endforeach
@endif

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="name">{{ __('lang.estateTypeName') }}</label>
    <div class="col-sm-10">
        <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') ?? $estateType->name ?? '' }}"/>
        <small class="form-text text-muted">The name of Estate Type</small>
    </div>
</div>

@csrf
