@extends('layouts.dashboard')

@section('content')


    <main class="cd-main-content">
        <nav class="cd-side-nav js-cd-side-nav">
            <h1>{{__('lang.dashboard')}}</h1>
            <ul class="cd-side__list js-cd-side__list">
{{--                <li class="cd-side__label"><span>Dashboard</span></li>--}}
                <li class="cd-side__item cd-side__item--has-children cd-side__item--overview js-cd-item--has-children">
                    <a href="{{ route('advertisement.index') }}">{{ __('lang.advertisement') }}</a>

{{--                    <ul class="cd-side__sub-list">--}}
{{--                        <li class="cd-side__sub-item"><a href="#0">All Data</a></li>--}}
{{--                        <!-- other list items here -->--}}
{{--                    </ul>--}}
                </li>

                <li class="cd-side__item cd-side__item--has-children cd-side__item--notifications cd-side__item--selected js-cd-item--has-children">
                    <a href="{{ route('EstateType.index') }}">{{ __('lang.department') }}</a>

{{--                    <ul class="cd-side__sub-list">--}}
{{--                        <li class="cd-side__sub-item"><a href="#0">All Notifications</a></li>--}}
{{--                        <!-- other list items here -->--}}
{{--                    </ul>--}}
                </li>

                <li class="cd-side__item cd-side__item--has-children cd-side__item--notifications cd-side__item--selected js-cd-item--has-children">
                    <a href="{{ route('areas.index') }}">{{ __('lang.area') }}</a>
                </li>

                <!-- other list items here -->
            </ul>

            <!-- other unordered lists here -->
        </nav>

        <div class="cd-content-wrapper">
            <!-- main content here -->
        </div> <!-- .cd-content-wrapper -->
    </main> <!-- .cd-main-content -->

@endsection
