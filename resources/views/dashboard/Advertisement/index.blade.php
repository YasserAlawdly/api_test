@extends('layouts.dashboard')

@section('buttons')
    <a class="btn btn-primary" href="{{route('advertisement.create')}}" role="button">{{ __('lang.addNewAdvertisement') }}</a>
@endsection

@section('content')
    <table class="table">
        <thead>
        <tr>
            <td>Advertisement ID</td>
            <td>Advertisement Name</td>
            <td>Estate Name</td>
            <th class="Actions">{{ __('lang.actions') }}</th>
        </tr>
        </thead>
        <body>
        @foreach($advertisements as $advertisement)
            <tr>
                <td>{{ $advertisement->id }}</td>
                <td>{{ __('advertisement.' . $advertisement->name) }}</td>
                <td>{{ $advertisement->estate->name }}</td>
                <td class="actions">
                    <a
                        href="{{ route('advertisement.edit', ['advertisement' => $advertisement->id]) }}"
                        alt="Edit"
                        title="Edit">
                        {{ __('lang.edit') }}
                    </a>
                    <form action="{{ route('advertisement.destroy', ['advertisement' => $advertisement->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-link" title="Delete" value="Delete">{{ __('lang.delete') }}</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </body>
    </table>
    {{ $advertisements->links() }}
@endsection
