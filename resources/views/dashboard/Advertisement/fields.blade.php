@if($errors->any())
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
    @endforeach
@endif

<div class="form-group row">
    <label class="col-sm-2 col-form-label"for="estate_id">{{ __('lang.estateName') }}</label>
    <div class="col-sm-10">
        <select name="estate_id" class="form-control" id="estate_id" required>
            @foreach($estates as $id => $display)
                <option value="{{ $id }}" {{ (isset($advertisement->estate_id) && $id === $advertisement->estate_id) ? 'selected' : '' }}>{{ $display }}</option>
            @endforeach
        </select>
        <small class="form-text text-muted">The Estate's advertisement.</small>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="name">{{ __('lang.advertisementName') }}</label>
    <div class="col-sm-10">
        <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') ?? $advertisement->name ?? '' }}"/>
        <small class="form-text text-muted">The name of advertisement</small>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="en">{{ __('lang.EnName') }}</label>
    <div class="col-sm-10">
        <input name="en" type="text" class="form-control" placeholder="Name" value="{{ old('en') ?? $advertisement->en ?? '' }}"/>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="ar">{{ __('lang.ArName') }}</label>
    <div class="col-sm-10">
        <input name="ar" type="text" class="form-control" placeholder="Name" value="{{ old('ar') ?? $advertisement->ar ?? '' }}"/>
    </div>
</div>

@csrf
