@extends('layouts.dashboard')

@section('content')
    <div class="col">
        <form action="{{ route('advertisement.update' , $advertisement) }}" method="POST">
            @method('PUT')

            @include('dashboard.Advertisement.fields');

            <div class="form-group row">
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary">{{ __('lang.addAdvertisement') }}</button>
                </div>
                <div class="col-sm-9">
                    <a href="{{ route('advertisement.index') }}" class="btn btn-secondary">{{ __('lang.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
@endsection
