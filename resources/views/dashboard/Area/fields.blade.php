@if($errors->any())
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
    @endforeach
@endif

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="name">{{ __('lang.AreaName') }}</label>
    <div class="col-sm-10">
        <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') ?? $area->name ?? '' }}"/>
        <small class="form-text text-muted">The name of Area</small>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="en">{{ __('lang.EnName') }}</label>
    <div class="col-sm-10">
        <input name="en" type="text" class="form-control" placeholder="Name" value="{{ old('en') ?? $area->en ?? '' }}"/>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="ar">{{ __('lang.ArName') }}</label>
    <div class="col-sm-10">
        <input name="ar" type="text" class="form-control" placeholder="Name" value="{{ old('ar') ?? $area->ar ?? '' }}"/>
    </div>
</div>

@csrf
