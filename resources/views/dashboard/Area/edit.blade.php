@extends('layouts.dashboard')

@section('content')
    <div class="col">
        <form action="{{ route('areas.update' , $area) }}" method="POST">
            @method('PUT')

            @include('dashboard.Area.fields');

            <div class="form-group row">
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary">{{ __('lang.addArea') }}</button>
                </div>
                <div class="col-sm-9">
                    <a href="{{ route('areas.index') }}" class="btn btn-secondary">{{ __('lang.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
@endsection
