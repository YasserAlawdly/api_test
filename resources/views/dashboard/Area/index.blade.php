@extends('layouts.dashboard')

@section('buttons')
    <a class="btn btn-primary" href="{{route('areas.create')}}" role="button">{{ __('lang.addNewArea') }}</a>
@endsection

@section('content')
    <table class="table">
        <thead>
        <tr>
            <td>Area ID</td>
            <td>Area Name</td>
            <td>Office Name</td>
            <th class="Actions">{{ __('lang.actions') }}</th>
        </tr>
        </thead>
        <body>
        @foreach($areas as $area)
            <tr>
                <td>{{ $area->id }}</td>
                <td>{{ __('area.' . $area->name) }}</td>
                <td>{{ $area->user->name }}</td>
                <td class="actions">
                    <a
                        href="{{ route('areas.edit', ['area' => $area->id]) }}"
                        alt="Edit"
                        title="Edit">
                        {{ __('lang.edit') }}
                    </a>
                    <form action="{{ route('areas.destroy', ['area' => $area->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-link" title="Delete" value="Delete">{{ __('lang.delete') }}</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </body>
    </table>
    {{ $areas->links() }}
@endsection
