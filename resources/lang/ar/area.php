<?php
$langs = \App\Area::all()->pluck('ar' , 'name');

$output = [];
foreach ($langs as $id => $value){
    $output[$id] = $value;
}
return $output;
