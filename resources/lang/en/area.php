<?php
$langs = \App\Area::all()->pluck('en' , 'name');

$output = [];
foreach ($langs as $id => $value){
    $output[$id] = $value;
}
return $output;
